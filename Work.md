# Risolvere indirezioni

## DONE
- Instrumentazione funzione
- Instrumentazione chiamate

## TODO
- Estendere algoritmo SVF affinchè:
    * Quando viene chiamata una funzione "instrumented" l'istruzione venga taggata con gli id delle def dei buffer passati
    * Quando si ha una use se il buffer utilizzato è un parametro venga taggata come una 
    * Far si che vengano considerate le  DEF che hanno una use "indiretta"

- Estendere zbouncer:
    * Instrumenti le chiamate taggate
    * Inserische le chiamate appropriate alla TA

## Problem
```C
int main(void){
    char buff1[10];
    char buff2[100];
    
    char *t;
    
    if(rand() % 2)
        t = buff1;
    else
        t = buff2;
    
    funct(t,t_id);
```