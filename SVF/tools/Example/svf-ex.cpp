#include "SVF-FE/LLVMUtil.h"
#include "Graphs/SVFG.h"
#include "WPA/Andersen.h"
#include "WPA/FlowSensitive.h"
#include "SVF-FE/PAGBuilder.h"
#include "Util/Options.h"
#include <iostream>
#include <fstream>
#include "DDA/FlowDDA.h"
#include "DDA/ContextDDA.h"
#include "DDA/DDAVFSolver.h"
#include "DDA/DDAPass.h"
#include "DDA/DDAStat.h"
#include "DDA/DDAClient.h"

using namespace llvm;
using namespace std;
using namespace SVF;

#define DEBUG
#define ARCH_PTR_SIZE 32

uint16_t idxCount = 0;

enum class TargetType {store_t, alloca_t};

std::vector<std::string> libFunctions = {"scanf", "__isoc99_scanf", "memcpy", "strcpy"};
std::vector<std::string> inputFunctions = {
    "wrapper_gets", "wrapper_getc", "wrapper_fgets", "fgetc", "wrapper_fread", "wrapper_scanf"};

bool ends_with(string str, string suffix){
    if(str.length() < suffix.length())
        return false;
    
    return !str.compare(str.length() - suffix.length(), suffix.length(), suffix);
}

void printType(Type *t){

    outs() << "Type: ";
    t->print(outs());
    outs() << "\n";

    outs() << "\tVoidType: " << t->isVoidTy() << "\n";
    outs() << "\tFunctionType: " << t->isFunctionTy() << "\n";
    outs() << "\tFirstClassType: " << t->isFirstClassType()<< "\n";
    outs() << "\t\tPointerType: " << t->isPointerTy() << "\n";
    outs() << "\t\tIntegerType: " << t->isIntegerTy() << "\n";
    outs() << "\t\tFloating-pointType: " << t->isFloatingPointTy() << "\n";
    outs() << "\tAggregateType: " << t->isAggregateType() << "\n";
    outs() << "\t\tArrayType: " << t->isArrayTy() << "\n";
    outs() << "\t\tStructType: " << t->isStructTy() << "\n";
}

unsigned int evaluateSizeDL(Type *t, Module *M, TargetType tt){
    #ifdef DEBUG
    outs() << "Called evaluate size on: ";
    printType(t);
    #endif  

    DataLayout dl =  DataLayout(M);

    unsigned int size;

    switch (tt)
    {
    case TargetType::store_t :
        size = dl.getTypeStoreSize(t);
        break;
    case TargetType::alloca_t :
        size = dl.getTypeAllocSize(t);
        break;
    }

    #ifdef DEBUG
    outs() << "Cost of allocating ";
    t->print(outs());
    outs() << " is " << size << " bytes\n";
    #endif

    return size;

}
/*
unsigned int evaluateSize(Type *t){
    #ifdef DEBUG
    outs() << "Called evaluate size on: ";
    printType(t);
    #endif

    if(ArrayType * innerType = dyn_cast<ArrayType>(t)){
        #ifdef DEBUG
        outs() << "DynCast to ArrayType [" << innerType->getArrayNumElements()
            << " x " << innerType->getArrayElementType()->getPrimitiveSizeInBits() << "]\n";
        #endif
        return innerType->getArrayNumElements() * innerType->getArrayElementType()->getPrimitiveSizeInBits();

    }else{
        if(StructType *innerType = dyn_cast<StructType>(t)){
            #ifdef DEBUG
            outs() << "DynCast to StructType\n";
            for (llvm::StructType::element_iterator it = innerType->element_begin(),
                eit = innerType->element_end(); it != eit; ++it){

                Type *tt = *it;
                outs() << "\t";
                tt->print(outs());
                outs() << " ";
            }
            #endif

            unsigned int total = 0;
            for (llvm::StructType::element_iterator it = innerType->element_begin(),
                eit = innerType->element_end(); it != eit; ++it){

                Type *tt = *it;
                total += evaluateSize(tt);
            }
            return total;

        }else{
            if(IntegerType *innerType = dyn_cast<IntegerType>(t)){
                #ifdef DEBUG
                outs() << "DynCast to IntegerType " << innerType->getPrimitiveSizeInBits() << "\n";
                #endif
                return innerType->getPrimitiveSizeInBits();
            }else{
                if(PointerType *innerType = dyn_cast<PointerType>(t)){
                    #ifdef DEBUG
                    outs() << "DynCast to PointerType\n";
                    #endif
                    return ARCH_PTR_SIZE;
                }else{
                    #ifdef DEBUG
                    outs() << "Type not handled (VoidType, FunctionType, Floating-pointType)\n";
                    #endif
                    return 0;
                }
            }
        }
    }
    
}
*/

Type *evaluateBaseType(Type *t){

    #ifdef DEBUG
    printType(t);
    #endif

    if(ArrayType * innerType = dyn_cast<ArrayType>(t)){
        #ifdef DEBUG
        outs() << "DynCast to ArrayType [" << innerType->getArrayNumElements()
            << " x " << innerType->getArrayElementType()->getPrimitiveSizeInBits() << "]\n";
        #endif
        return innerType;

    }else{
        if(StructType *innerType = dyn_cast<StructType>(t)){
            #ifdef DEBUG
            outs() << "DynCast to StructType\n";
            for (llvm::StructType::element_iterator it = innerType->element_begin(),
                eit = innerType->element_end(); it != eit; ++it){

                Type *tt = *it;
                outs() << "\t";
                tt->print(outs());
                outs() << " ";
            }
            #endif

            return innerType;

        }else{
            if(IntegerType *innerType = dyn_cast<IntegerType>(t)){
                #ifdef DEBUG
                outs() << "DynCast to IntegerType " << innerType->getPrimitiveSizeInBits() << "\n";
                #endif
                return innerType;
            }else{
                if(PointerType *innerType = dyn_cast<PointerType>(t)){

                    #ifdef DEBUG
                    outs() << "DynCast to PointerType\n";
                    #endif
                    if(innerType->isIntegerTy())
                        return evaluateBaseType(innerType);
                    else
                        return evaluateBaseType(innerType->getPointerElementType());

                    
                }else{
                    #ifdef DEBUG
                    outs() << "Type not handled (VoidType, FunctionType, Floating-pointType)\n";
                    #endif
                    return nullptr;
                }
            }
        }
    }
    
}

class Def {
    private:
    uint16_t idx;
    SVFGNode *defSVFGNode;
    AllocaInst *defAlloca;

    public:
    Def(SVFGNode *n, AllocaInst *a){
        defSVFGNode = n;
        defAlloca = a;
        idx = idxCount;
        idxCount++;
    }

    uint16_t getIdx(){ return idx; }

    SVFGNode *getDefSVFGNode(){ return defSVFGNode; }

    AllocaInst *getDefAlloca(){ return defAlloca; }

    unsigned int getTotalBytes(){
        return evaluateSizeDL(defAlloca->getAllocatedType(),LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule(),TargetType::alloca_t);// / 8;
    }

    Type *getType(){ return defAlloca->getAllocatedType(); }

    friend SVF::raw_fd_ostream & operator<<(SVF::raw_fd_ostream & _stream, Def const & mc) {
        _stream << "[" << mc.idx << ".DEF] (PAGNode: " << mc.defSVFGNode->getId() << ") " << *mc.defSVFGNode;
        return _stream;
    }

};

class StoreUse{
    private:
    uint16_t idx;
    VFGNode *use;
    StoreInst *storeUse;
    Value *storeParamValue;
    std::vector<Def *> targetObjects;

    public:
    StoreUse(VFGNode *n, StoreInst *i){
        use = n;
        idx = idxCount;
        idxCount++;
        storeUse = i;
    }

    uint16_t getIdx(){ return idx; }

    VFGNode *getUseVFGNode(){ return use; }

    StoreInst *getStoreUse(){ return storeUse; }

    Value *getStoreParamSVFGNode(){ return storeParamValue; }
    void setStoreParamSVFGNode(Value *v){ storeParamValue = v; }

    void addTargetObject(Def *d){
        targetObjects.emplace_back(d);
    }

    int countTargetObject(){ return targetObjects.size(); }

    std::vector<Def *>::iterator targetObjBegin(){ return targetObjects.begin(); }
    std::vector<Def *>::iterator targetObjEnd(){ return targetObjects.end(); }

    Type *getStoreTargetType(){

        Value *ptrStoreOperand = storeUse->getPointerOperand();

        #ifdef DEBUG
        outs() << "Pointer operand of store: ";
        ptrStoreOperand->print(outs());
        outs() << "\n";
        #endif

        if (llvm::GetElementPtrInst *gep = dyn_cast<llvm::GetElementPtrInst>(ptrStoreOperand)) {
            #ifdef DEBUG
            outs() << "GEP, getSourceElementType(): ";
            gep->getSourceElementType()->print(outs());
            outs() << "\n";
            #endif

            Type *baseType = evaluateBaseType(gep->getSourceElementType());
            #ifdef DEBUG
            outs() << "BaseType: ";
            baseType->print(outs());
            outs() << "\n";
            #endif

            return baseType;
            
        }else{
            if (llvm::AllocaInst *a = dyn_cast<llvm::AllocaInst>(ptrStoreOperand)) {
                //Type *baseType = evaluateBaseType(a->getAllocatedType());
                Type *baseType = a->getAllocatedType();
                #ifdef DEBUG
                outs() << "BaseType x: ";
                baseType->print(outs());
                outs() << "\n";
                #endif

                return baseType;

            }else{
                outs() << "Strange situation, the storeParam is not GEP nor Alloca." << "\n";
                errs() << "Cannot fill the size of memory object: yield!" << "\n";
                return nullptr;
            }
        }
    
    }

    friend SVF::raw_fd_ostream & operator<<(SVF::raw_fd_ostream & _stream, StoreUse const & mc) {
        _stream << "[" << mc.idx << ".STOREUSE] (VFGNode: " << *mc.use << ") " << "\n" <<
            " storeParam:" << *mc.storeParamValue << "\n" << "Count target object: " << mc.targetObjects.size();
        return _stream;
    }

    unsigned int getTotalBytesOfBaseType(){
        return evaluateSizeDL(getStoreTargetType(),LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule(),TargetType::store_t);// / 8;
    }

};

class CallLibUse{
    private:
    uint16_t idx;
    CallBlockNode *callBlockNode;
    CallBase *call;
    Instruction *callTargetParam;
    std::vector<Def *> callTargetObjects; // must be set by the analyzer

    public:
    CallLibUse(CallBlockNode *n){
        idx = idxCount;
        idxCount++;
        callBlockNode = n;
        call = (CallBase *) callBlockNode->getCallSite();
    }

    uint16_t getIdx(){ return idx; }

    CallBase *getCall(){ return call; }

    CallBlockNode *getCallBlockNode(){ return callBlockNode; }

    void setCallTargetParam(Instruction *i){ callTargetParam = i; }

    void addTargetObject(Def *d){
        callTargetObjects.emplace_back(d);
    }

    int countTargetObject(){ return callTargetObjects.size(); }

    std::vector<Def *>::iterator targetObjBegin(){ return callTargetObjects.begin(); }
    std::vector<Def *>::iterator targetObjEnd(){ return callTargetObjects.end(); }

    Type *getCallTargetType(){

        #ifdef DEBUG
        outs() << "CallTargetObject: ";
        //callTargetObjects
        outs() << "\n";
        #endif

        if (llvm::GetElementPtrInst *gep = dyn_cast<llvm::GetElementPtrInst>(callTargetParam)) {
            #ifdef DEBUG
            outs() << "GEP, getSourceElementType(): ";
            gep->getSourceElementType()->print(outs());
            outs() << "\n";
            #endif

            Type *baseType = evaluateBaseType(gep->getSourceElementType());
            #ifdef DEBUG
            outs() << "BaseType: ";
            baseType->print(outs());
            outs() << "\n";
            #endif

            return baseType;
            
        }else{
            if (llvm::AllocaInst *a = dyn_cast<llvm::AllocaInst>(callTargetParam)) {
                Type *baseType = evaluateBaseType(a->getAllocatedType());
                #ifdef DEBUG
                outs() << "BaseType: ";
                baseType->print(outs());
                outs() << "\n";
                #endif

                return baseType;

            }else{
                outs() << "Strange situation, the storeParam is not GEP nor Alloca." << "\n";
                errs() << "Cannot fill the size of memory object: yield!" << "\n";
                return callTargetParam->getType();
            }
        }
    
    }

    friend SVF::raw_fd_ostream & operator<<(SVF::raw_fd_ostream & _stream, CallLibUse const & mc) {
        _stream << "[" << mc.idx << ".LIBUSE] " << " Call:" << *mc.call;
        return _stream;
    }

    unsigned int getTotalBytesOfBaseType(){
        return evaluateSizeDL(getCallTargetType(),LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule(),TargetType::store_t);// / 8;
    }

};

class In {
    private:
    uint16_t idx;
    PAGNode *targetPAGNode;
    CallBlockNode *blacklistedCall;
    std::vector<Def *> defs;
    std::vector<StoreUse *> storeUse;
    std::vector<CallLibUse *> callLibUse;

    public:
    In(PAGNode *n){
        targetPAGNode = n;
        idx = idxCount;
        idxCount++;
    }
    
    uint16_t getIdx(){ return idx; }

    PAGNode *getInPAGNode(){ return targetPAGNode; }
    
    CallBlockNode *getBlacklistedCall(){ return blacklistedCall; }
    void setBlacklistedCall(CallBlockNode *node){ blacklistedCall = node; }

    CallBase *getCallInputFunction(){ return (CallBase *)blacklistedCall->getCallSite(); }

    void defInsert(Def *d){ defs.emplace_back(d); }
    std::vector<Def *>::iterator defBegin(){ return defs.begin(); }
    std::vector<Def *>::iterator defEnd(){ return defs.end(); }

    void storeUseInsert(StoreUse *u){ storeUse.emplace_back(u); }
    std::vector<StoreUse *>::iterator storeUseBegin(){ return storeUse.begin(); }
    std::vector<StoreUse *>::iterator storeUseEnd(){ return storeUse.end(); }

    void callLibUseInsert(CallLibUse *u){ callLibUse.emplace_back(u); }
    std::vector<CallLibUse *>::iterator callLibUseBegin(){ return callLibUse.begin(); }
    std::vector<CallLibUse *>::iterator callLibUseEnd(){ return callLibUse.end(); }

    int defCount(){ return defs.size(); }

    friend SVF::raw_fd_ostream& operator<<(SVF::raw_fd_ostream& _stream, const In& mc)
    {
        CallBase *call = (CallBase*) mc.blacklistedCall->getCallSite();
        _stream << "[" << mc.idx << ".IN] (PAGNode: " << mc.targetPAGNode->getId()
            << ") " << call->getCalledFunction()->getName().str();
        return _stream;
    }

};

static llvm::cl::opt<std::string> InputFilename(cl::Positional, llvm::cl::desc("<input bitcode>"), llvm::cl::init("-"));

/*!
 * An example to query alias results of two LLVM values
 */
AliasResult aliasQuery(PointerAnalysis* pta, Value* v1, Value* v2)
{
    return pta->alias(v1,v2);
}

/*!
 * An example to print points-to set of an LLVM value
 */
std::string printPts(PointerAnalysis* pta, Value* val)
{

    std::string str;
    raw_string_ostream rawstr(str);

    NodeID pNodeId = pta->getPAG()->getValueNode(val);
    const NodeBS& pts = pta->getPts(pNodeId);
    for (NodeBS::iterator ii = pts.begin(), ie = pts.end();
            ii != ie; ii++)
    {
        rawstr << " " << *ii << " ";
        PAGNode* targetObj = pta->getPAG()->getPAGNode(*ii);
        if(targetObj->hasValue())
        {
            rawstr << "(" <<*targetObj->getValue() << ")\t ";
        }
    }

    return rawstr.str();

}


/*!
 * An example to query/collect all successor nodes from a ICFGNode (iNode) along control-flow graph (ICFG)
 */
void traverseOnICFG(ICFG* icfg, const Instruction* inst)
{
    ICFGNode* iNode = icfg->getBlockICFGNode(inst);
    FIFOWorkList<const ICFGNode*> worklist;
    Set<const ICFGNode*> visited;
    worklist.push(iNode);

    /// Traverse along VFG
    while (!worklist.empty())
    {
        const ICFGNode* vNode = worklist.pop();
        for (ICFGNode::const_iterator it = iNode->OutEdgeBegin(), eit =
                    iNode->OutEdgeEnd(); it != eit; ++it)
        {
            ICFGEdge* edge = *it;
            ICFGNode* succNode = edge->getDstNode();
            if (visited.find(succNode) == visited.end())
            {
                visited.insert(succNode);
                worklist.push(succNode);
            }
        }
    }
}

std::vector<VFGNode *> *VFGNodeFilter(std::vector<VFGNode *> *s, VFGNode::VFGNodeK kind){

    std::vector<VFGNode *> *result = new vector<VFGNode *>();

    for(auto it = s->begin(), eit = s->end(); it!=eit; ++it){
        VFGNode* node = *it;
        if(node->getNodeKind() == kind){
            #ifdef DEBUG
            outs() << "VFG-ID=" << node->getId() << " (store) | " << node->getICFGNode()->toString() << "\n";
            #endif
            result->emplace_back(node);
            
        }
    }

    return result;
}

/*!
 * An example to query/collect all the uses of a definition of a value along value-flow graph (VFG)
 */
std::vector<VFGNode*> *traverseOnVFG(const SVFG* vfg, Value* val){
    PAG* pag = PAG::getPAG();
    
    PAGNode* pNode = pag->getPAGNode(pag->getValueNode(val));
    const VFGNode* vNode = vfg->getDefSVFGNode(pNode);
    FIFOWorkList<const VFGNode*> worklist;
    Set<const VFGNode*> visited;
    worklist.push(vNode);

    /// Traverse along VFG
    while (!worklist.empty())
    {
        const VFGNode* vNode = worklist.pop();
        for (VFGNode::const_iterator it = vNode->OutEdgeBegin(), eit =
                    vNode->OutEdgeEnd(); it != eit; ++it)
        {
            VFGEdge* edge = *it;
            VFGNode* succNode = edge->getDstNode();
            if (visited.find(succNode) == visited.end())
            {
                visited.insert(succNode);
                worklist.push(succNode);
            }
        }
    }

    // convert in std::set with non-const elements
    std::vector<VFGNode *> *result = new vector<VFGNode *>();
    for(Set<const VFGNode*>::const_iterator it = visited.begin(), eit = visited.end(); it!=eit; ++it){
        const VFGNode* node = *it;
        result->emplace_back(const_cast<VFGNode *>(node));
    }

    return result;

}

std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *findDefNodesBackwardFromSVFGNode(SVFGNode *baseNode){

    FIFOWorkList<const SVFGNode *> worklist;
    Set<const SVFGNode*> visited;
    std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *defsResult = new vector<std::tuple<SVFGNode *, AllocaInst *> *>();
    std::set<AllocaInst *> *dupplicateMirror = new set<AllocaInst *>();
    worklist.push(baseNode);

    if(baseNode->getId() != 1){
        Value *v = const_cast<Value *>(baseNode->getValue());

        if(v != nullptr){
            if(Instruction *i = dyn_cast<Instruction>(v)){
                if (AllocaInst *alloca = dyn_cast<AllocaInst>(i)) {
                    dupplicateMirror->insert(alloca);

                    std::tuple<SVFGNode *, AllocaInst *> *t = new tuple<SVFGNode *, AllocaInst *>(baseNode, alloca);
                    defsResult->emplace_back(t);
                    #ifdef DEBUG
                    outs() << "ADD alloca: " << *baseNode << "\n";
                    #endif
                }
            }
        }
        
    }

    while (!worklist.empty()){
        const SVFGNode* node = worklist.pop();

        for (SVFGNode::const_iterator it = node->InEdgeBegin(), eit = node->InEdgeEnd(); it != eit; ++it){
            SVFGEdge* edge = *it;
            SVFGNode* preNode = edge->getSrcNode();

            if(preNode->getId() != 1){
                Value *v = const_cast<Value *>(preNode->getValue());
                if(v != nullptr){
                    if(Instruction *i = dyn_cast<Instruction>(v)){
                        
                        if (AllocaInst *alloca = dyn_cast<AllocaInst>(i)) {

                            if(dupplicateMirror->count(alloca) == 0){
                                dupplicateMirror->insert(alloca);

                                std::tuple<SVFGNode *, AllocaInst *> *t = new tuple<SVFGNode *, AllocaInst *>(preNode, alloca);
                                defsResult->emplace_back(t);

                                #ifdef DEBUG
                                outs() << "ADD alloca: " << *preNode << "\n";
                                #endif
                            }else{
                                #ifdef DEBUG
                                outs() << "NO alloca already present: " << *preNode << "\n";
                                #endif
                            }
                        }
                    }
                }
                
            }

            if (visited.find(preNode) == visited.end()){
                visited.insert(preNode);
                worklist.push(preNode);
            }

        }
    }

    return defsResult;

}

PAGNode *recursiveFindFirstStore(PAGNode *top){
    vector<PAGNode *>lower;

    for(const PAGEdge *outEdge : top->getOutEdges()){
        if(outEdge->getEdgeKind() == PAGEdge::Store)
            return outEdge->getDstNode();
        else
            lower.emplace_back(outEdge->getDstNode());
    }

    for(PAGNode *l : lower)
        return recursiveFindFirstStore(l);

    return nullptr;
}

void exportTrace(std::vector<In *> &trace, LLVMContext &ctx, string traceName){

    char text[100];
    std::error_code OutErrorInfo;
    llvm::raw_fd_ostream exportFile(llvm::StringRef(traceName + ".ztrace"), OutErrorInfo, llvm::sys::fs::F_None);
    std::map<Instruction *, std::tuple<string *, string *, string *>> instrMap; // <instr, alloca, store, lib>

    for(In *i : trace){
        outs() << *i << "\n";

        std::string out = i->getBlacklistedCall()->toString(); 
        std::replace( out.begin(), out.end(), '\n', ' '); 
        exportFile << i->getIdx() << ";IN;" << ";" << out << "\n";

        // Definition from the current Input function, not usefull for the trace, usefull only for the analisys
        for (auto it = i->defBegin(), eit = i->defEnd(); it != eit; ++it){
            Def *d = *it;

            std::string out = d->getDefSVFGNode()->toString(); 
            std::replace( out.begin(), out.end(), '\n', ' '); // trim \n from SVFGNode->toString()
            exportFile << d->getIdx() << ";DEF;" << i->getIdx() << ";" 
                <<  evaluateSizeDL(d->getDefAlloca()->getAllocatedType(),LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule(),TargetType::alloca_t) << ";" << out << "\n";
        }

        // StoreUse of from the current Input function
        for (auto it = i->storeUseBegin(), eit = i->storeUseEnd(); it != eit; ++it){
            StoreUse *u = *it;

            sprintf(text, "%d", u->getIdx());
            Instruction *instr = (Instruction *)u->getStoreUse();
            if(instrMap.count(instr) == 0){
                instrMap.insert({instr, tuple<string *, string *, string *>(new string(""), new string(text), new string(""))});
            }else{
                auto elem = instrMap.find(instr);
                std::tuple<string *, string *, string *> s = elem->second;
                string *allocaStr; string *storeStr; string *libStr;
                std::tie(allocaStr, storeStr, libStr) = s; 
                sprintf(text, "_%d", u->getIdx());
                storeStr->append(text);
            }
            
            std::string out = u->getUseVFGNode()->toString();
            std::replace( out.begin(), out.end(), '\n', ' '); 
            exportFile << "\t" << u->getIdx() << ";SUSE;" << i->getIdx() << ";" << out << "\n";


            // Definition target of the current StoreUse
            for (auto it = u->targetObjBegin(), eit = u->targetObjEnd(); it != eit; ++it){
                Def *d = *it;
                
                sprintf(text, "%d", d->getIdx());
                Instruction *instr = (Instruction *)d->getDefAlloca();
                if(instrMap.count(instr) == 0){
                    instrMap.insert({instr, tuple<string *, string *, string *>(new string(text), new string(""), new string(""))});
                }else{
                    auto elem = instrMap.find(instr);
                    std::tuple<string *, string *, string *> s = elem->second;
                    string *allocaStr; string *storeStr; string *libStr;
                    std::tie(allocaStr, storeStr, libStr) = s; 
                    sprintf(text, "_%d", d->getIdx());
                    allocaStr->append(text);
                }

                std::string out = d->getDefSVFGNode()->toString();
                std::replace( out.begin(), out.end(), '\n', ' '); 
                exportFile << "\t\t" << d->getIdx() << ";SDEF;" << u->getIdx() << ";" << d->getTotalBytes() << ";" << out << "\n";
            }
        }

        // LibUse (memcpy/strcpy/scanf..) of from the current Input function
        for (auto it = i->callLibUseBegin(), eit = i->callLibUseEnd(); it != eit; ++it){
            CallLibUse *u = *it;

            sprintf(text, "%d", u->getIdx());
            Instruction *instr = (Instruction *)u->getCall();
            if(instrMap.count(instr) == 0){
                instrMap.insert({instr, tuple<string *, string *, string *>(new string(""), new string(""), new string(text))});
            }else{
                auto elem = instrMap.find(instr);
                std::tuple<string *, string *, string *> s = elem->second;
                string *allocaStr; string *storeStr; string *libStr;
                std::tie(allocaStr, storeStr, libStr) = s; 
                sprintf(text, "_%d", u->getIdx());
                libStr->append(text);
            }

            std::string out = u->getCallBlockNode()->toString();
            std::replace( out.begin(), out.end(), '\n', ' '); 
            exportFile << "\t" << u->getIdx() << ";LUSE;" << i->getIdx() << ";" << out << "\n";

            // Definition target of the current LibUse
            for (auto it = u->targetObjBegin(), eit = u->targetObjEnd(); it != eit; ++it){
                Def *d = *it;
                
                sprintf(text, "%d", d->getIdx());
                Instruction *instr = (Instruction *)d->getDefAlloca();
                if(instrMap.count(instr) == 0){
                    instrMap.insert({instr, tuple<string *, string *, string *>(new string(text), new string(""), new string(""))});
                }else{
                    auto elem = instrMap.find(instr);
                    std::tuple<string *, string *, string *> s = elem->second;
                    string *allocaStr; string *storeStr; string *libStr;
                    std::tie(allocaStr, storeStr, libStr) = s; 
                    sprintf(text, "_%d", d->getIdx());
                    allocaStr->append(text);
                }

                std::string out = d->getDefSVFGNode()->toString();
                std::replace( out.begin(), out.end(), '\n', ' '); 
                exportFile << "\t\t" << d->getIdx() << ";LDEF;" << u->getIdx() << ";" << d->getTotalBytes() << ";" << out << "\n";
            }


        }
        
    }

    exportFile.close();

    for(auto it = instrMap.cbegin(); it != instrMap.cend(); ++it){
        std::tuple<string *, string *, string *> s = it->second;
        std::string *allocaStr; std::string *storeStr; string* libStr;
        std::tie(allocaStr, storeStr, libStr) = s; 
        Instruction *instr = it->first;
        outs() << *instr << " | allocaStr: " << *allocaStr << " | storeStr: " << *storeStr << " | libStr: " << *libStr << "\n";

        if(!allocaStr->empty())
            instr->setMetadata("zdef", MDNode::get(ctx, MDString::get(ctx, *allocaStr)));

        if(!storeStr->empty())
            instr->setMetadata("zsuse", MDNode::get(ctx, MDString::get(ctx, *storeStr)));

        if(!libStr->empty())
            instr->setMetadata("zluse", MDNode::get(ctx, MDString::get(ctx, *libStr)));
    }

}

Argument *check_parameter_aliasing(const Value *param,Function *func, PointerAnalysis *pta) {
    for(llvm::Function::arg_iterator a = func->arg_begin(); a != func->arg_end(); a++ ){
        //Argument arg = *a;
        outs() << "Checking aliasing of " << param->getName() << " with argument " << (*a).getName() << " of " << func->getName() << "\n";
        if(pta->alias(param, &(*a)) == AliasResult::NoAlias)
            continue;
        
        outs() << "Parameter " << param->getName() << " is aliased with argument " << (*a).getName() << "\n";
        return &(*a);
    }

    return NULL;
}

int get_def_id(std::vector<In *> trace, SVFGNode *node){
    //Cerco tra tutte le definizioni così da inserire l'id appropriato, ritorna -1 in caso non venga trovata la definizione
    for(In *i : trace){
        // REFACTORING NEEDED
        for (auto it = i->storeUseBegin(); it != i->storeUseEnd(); ++it){
            StoreUse *u = *it;
            for(auto it = u->targetObjBegin(); it != u->targetObjEnd(); it++ ){
                Def *d = *it;
                if(node->getId() == d->getDefSVFGNode()->getId())
                    return d->getIdx();
            }
        }

        for (auto it = i->callLibUseBegin(); it != i->callLibUseEnd(); it++){
            CallLibUse *l = *it;
            for(auto it = l->targetObjBegin(); it != l->targetObjEnd(); it++ ){
                Def *d = *it;
                if(node->getId() == d->getDefSVFGNode()->getId())
                    return d->getIdx();
            }
        }

        for (auto it = i->defBegin(); it != i->defEnd(); it++){
            Def *d = *it;
            if(node->getId() == d->getDefSVFGNode()->getId())
                return d->getIdx();
        }
    }

    return -1;
}

int main(int argc, char ** argv){

   int arg_num = 0;
    char **arg_value = new char*[argc];
    std::vector<std::string> moduleNameVec;
    SVFUtil::processArguments(argc, argv, arg_num, arg_value, moduleNameVec);
    //cl::ParseCommandLineOptions(arg_num, arg_value,
    //"Whole Program Points-to Analysis\n");
    
    if (Options::WriteAnder == "ir_annotator")
    {
        LLVMModuleSet::getLLVMModuleSet()->preProcessBCs(moduleNameVec);
    }

    SVF::BVDataPTAImpl *pta = NULL;

    SVFModule* svfModule = LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(moduleNameVec);
    svfModule->buildSymbolTableInfo();

	/// Build Program Assignment Graph (PAG)
	PAGBuilder builder;
	PAG* pag = builder.build(svfModule);

    string ptaName = "";
    string argNameDDA = "--pta-dda";
    string argNameFS = "--pta-fs";
    string argNameAnder = "--pta-ander";

    if(argc >= 2){

        outs() << "args PTA type: " << argv[2] << "\n";
        if (argNameDDA.find(string(argv[2])) != std::string::npos){ //s.compare(t)
            ptaName = "dda";
            DDAClient *client = new DDAClient(svfModule);
            client->initialise(svfModule);
            FlowDDA *dda = new FlowDDA(pag, client);
            pta = dda;

            dda->initialize();
            client->answerQueries(dda);
        }else if(argNameAnder.find(string(argv[2])) != std::string::npos){
            ptaName = "ander";
            Andersen* ander = AndersenWaveDiff::createAndersenWaveDiff(pag);
            pta = ander;
        }else if (argNameFS.find(string(argv[2])) != std::string::npos){
            ptaName = "fs";
            FlowSensitive *fs = FlowSensitive::createFSWPA(pag);
            pta = fs;
        }else{
            outs() << "No PTA set, exit" << "\n";
            exit(1);
        }
    }

    PAG::CallSiteSet callSite = pag->getCallSiteSet();
    
    outs() << "---------------------------------------------------" << "\n";

    std::vector<In *> trace;

    //Zbouncer *zbouncer = new Zbouncer();
    Module *m = LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule();
    //zbouncer->runOnModule(m);

    /// Call Graph
    PTACallGraph* callgraph = pta->getPTACallGraph();

    /// ICFG
    ICFG* icfg = pag->getICFG();

    /// Value-Flow Graph (VFG)
    VFG* vfg = new VFG(callgraph);

    /// Sparse value-flow graph (SVFG)
    SVFGBuilder svfBuilder;
    SVFG* svfg = svfBuilder.buildFullSVFG(pta);


    for(PAG::CallSiteSet::iterator it = callSite.begin(); it != callSite.end(); ++it){
        const CallBlockNode *callBlockNode = *it;
        CallBase *call = (CallBase*) callBlockNode->getCallSite();

        const ICFGNode *nodz = (ICFGNode *)callBlockNode;
        

        if(call->getCalledFunction() == 0){
            outs() << "Dropping " << call->getName().str() << " because getCalledFunction is nullptr" << "\n";
            continue;
        }
        
        if (std::find(inputFunctions.begin(), inputFunctions.end(), call->getCalledFunction()->getName()) != inputFunctions.end()){
                outs() << "(" << call->getCalledFunction()->getName().str() << ") " << callBlockNode->toString() <<  "\n";

                std::vector<const PAGNode *>paramList = callBlockNode->getActualParms();
                const PAGNode *functionParam;

                const PAGNode *ret = callBlockNode->getRetBlockNode()->getActualRet();

                if(call->getCalledFunction()->getName().str() == "wrapper_scanf")
                    functionParam = paramList[1];
                
                if(call->getCalledFunction()->getName().str() == "fgetc"){
                    functionParam = ret;
                    if(ret->getOutEdges().size() == 0){
                        outs() << "ERROR: fgetc call PAG::" << ret->getId() << " doesn't have an outgoing store edge" << "\n";
                    }else{
                        functionParam = recursiveFindFirstStore((PAGNode *)ret);
                    }
                }

                In *in = new In(const_cast<PAGNode *>(functionParam));
                trace.emplace_back(in);
                
                in->setBlacklistedCall(const_cast<CallBlockNode *>(callBlockNode));

                const SVFGNode *defOfParam = svfg->getDefSVFGNode(functionParam);
                std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *defsResult = findDefNodesBackwardFromSVFGNode(const_cast<SVFGNode *>(defOfParam));

                for (auto it = defsResult->begin(), eit = defsResult->end(); it != eit; ++it){
                    std::tuple<SVFGNode *, AllocaInst *> *t = *it;
                    SVFGNode *node; AllocaInst *alloca;
                    std::tie(node, alloca) = *t; 
                    Def *d = new Def(node, alloca);
                    in->defInsert(d);

                    std::vector<VFGNode *> *allUseNodes = traverseOnVFG(svfg, (Value *)alloca);
                    std::vector<VFGNode *> *storeUse = VFGNodeFilter(allUseNodes, VFGNode::Store);

                    for (vector<VFGNode *>::iterator it = storeUse->begin(), eit = storeUse->end(); it != eit; ++it){
                        VFGNode *storeNode = *it;
                        StoreInst *store = (StoreInst *)storeNode->getValue();

                        StoreUse *u = new StoreUse(storeNode, store);
                        u->setStoreParamSVFGNode(store->getPointerOperand());
                        in->storeUseInsert(u);

                        Value *where = store->getOperand(1);
                        #ifdef DEBUG
                        outs() << "Store: " << *store << "\n";
                        outs() << "Store where: " << *where << "\n";
                        #endif

                        for(const SVFGNode *node : svfg->fromValue(storeNode->getValue())){
                            for(const PAGEdge *edge : node->getICFGNode()->getPAGEdges()){
                                SVFGNode *defOfStoreNode = const_cast<SVFGNode *>(svfg->getDefSVFGNode(edge->getDstNode()));
                                #ifdef DEBUG
                                outs() << "Def site: " << *defOfStoreNode << "\n";
                                #endif

                                std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *defsStoreResult = findDefNodesBackwardFromSVFGNode(defOfStoreNode);
                                for (auto it = defsStoreResult->begin(), eit = defsStoreResult->end(); it != eit; ++it){
                                    std::tuple<SVFGNode *, AllocaInst *> *t = *it;
                                    SVFGNode *node; AllocaInst *alloca;
                                    std::tie(node, alloca) = *t; 
                                    Def *d = new Def(node, alloca);
                                    u->addTargetObject(d);
                                }
                            }
                        }
                    }
                   
                }
            }  

        if(std::find(libFunctions.begin(), libFunctions.end(), call->getCalledFunction()->getName()) != libFunctions.end()){
                outs() << "(lib: " << call->getCalledFunction()->getName().str() << ") " << callBlockNode->toString() <<  "\n";

                std::vector<const PAGNode *>paramList = callBlockNode->getActualParms();
                const PAGNode *functionParam;

                const PAGNode *ret = callBlockNode->getRetBlockNode()->getActualRet();

                if(call->getCalledFunction()->getName().str() == "scanf")
                    functionParam = paramList[1];
                if(call->getCalledFunction()->getName().str() == "__isoc99_scanf")
                    functionParam = paramList[1];
                if(call->getCalledFunction()->getName().str() == "strcpy")
                    functionParam = paramList[0];
                if(call->getCalledFunction()->getName().str() == "memcpy")
                    functionParam = paramList[0];

                In *in = new In(const_cast<PAGNode *>(functionParam));
                trace.emplace_back(in);
                
                in->setBlacklistedCall(const_cast<CallBlockNode *>(callBlockNode));

                const SVFGNode *defOfParam = svfg->getDefSVFGNode(functionParam);
                std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *defsResult = findDefNodesBackwardFromSVFGNode(const_cast<SVFGNode *>(defOfParam));

                for (auto it = defsResult->begin(), eit = defsResult->end(); it != eit; ++it){
                    std::tuple<SVFGNode *, AllocaInst *> *t = *it;
                    SVFGNode *node; AllocaInst *alloca;
                    std::tie(node, alloca) = *t; 
                    Def *d = new Def(node, alloca);
                    in->defInsert(d);

                    CallLibUse *clu = new CallLibUse(const_cast<CallBlockNode *>(callBlockNode));
                    clu->addTargetObject(d);
                    clu->setCallTargetParam(alloca);
                    in->callLibUseInsert(clu);

                }

            }

        
    }

    for(In *i : trace){
        outs() << *i << "\n";

        for (auto it = i->defBegin(), eit = i->defEnd(); it != eit; ++it){
            Def *d = *it;
            outs() << *d << "\n";

            printType(evaluateBaseType(d->getType()));

            for (auto it = i->storeUseBegin(), eit = i->storeUseEnd(); it != eit; ++it){
                StoreUse *u = *it;
                outs() << *u << "\n";

                for (auto it = u->targetObjBegin(), eit = u->targetObjEnd(); it != eit; ++it){
                    Def *d = *it;
                    outs() << *d << "\n";

                    //#ifdef DEBUG
                    outs() << "EvaluateType: " << "\n";
                    printType(evaluateBaseType(d->getType()));
                    outs() << "Total bytes: " << d->getTotalBytes() << "\n";
                    //#endif
                }
            }

            for (auto it = i->callLibUseBegin(), eit = i->callLibUseEnd(); it != eit; ++it){
                CallLibUse *u = *it;
                outs() << *u << "\n";

                //#ifdef DEBUG
                outs() << *evaluateBaseType(u->getCallTargetType()) << "\n";
                outs() << "Total bytes: " << u->getTotalBytesOfBaseType() << "\n";
                //#endif
            }
        }
    }

    exportTrace(trace, LLVMModuleSet::getLLVMModuleSet()->getContext(), string(argv[1]) + "." + ptaName);

    /*
    Parte Rusconi
    */

    
    LLVMContext &ctx = LLVMModuleSet::getLLVMModuleSet()->getContext();

    

    for(PAG::CallSiteSet::iterator cs = callSite.begin(); cs != callSite.end(); cs++){
        const CallBlockNode *callBlockNode = *cs;
        CallBase *callee = (CallBase*) callBlockNode->getCallSite();
        Function *caller = callee->getCaller();
        
        if(callee->getCalledFunction() == 0)
            continue;
        
        string func_name = string(callee->getCalledFunction()->getName());
        outs() << " Analyzing call to " << func_name << " inside " <<  callee->getCaller()->getName() << "\n" ; 

        bool instrumented = ends_with(func_name, "_instrumented");

        if(!instrumented){
            continue;
        }
    
        outs() << "Function " << func_name << "is instrumented\n";
        
        std::vector<const PAGNode *>paramList = callBlockNode->getActualParms();

        for( int i = 0; i < paramList.size() ; i++){
            if(!paramList[i]->isPointer()){
                outs() << "Parameter " << i << " is not a pointer\n";
                continue;
            }

            outs() << "Parameter " << i <<  " is a pointer\n";


            llvm::Argument *arg;
            if (paramList[i]->hasValue() && ((arg = check_parameter_aliasing(paramList[i]->getValue(),caller,pta)) != NULL)){
                string name = "zref_" + string(m->getFunction(func_name)->getArg(i)->getName()) + "_id";
                string value = string((*arg).getName()) + "_id";
                callee->setMetadata(name,MDNode::get(ctx, MDString::get(ctx, value)));

                continue;
            }
                

            const SVFGNode *defOfParam = svfg->getDefSVFGNode(paramList[i]);
            outs() << defOfParam->toString() << "\n";
            std::vector<std::tuple<SVFGNode *, AllocaInst *> *> *defsResult = findDefNodesBackwardFromSVFGNode(const_cast<SVFGNode *>(defOfParam));
            outs() << "Defs of param " << i << " are " << defsResult->size() << "\n";

            for(auto it = defsResult->begin(); it != defsResult->end(); it++ ){
                std::tuple<SVFGNode *, AllocaInst *> *t = *it;
                SVFGNode *node; AllocaInst *alloca;
                std::tie(node, alloca) = *t; 
                

                printType(alloca->getAllocatedType());
       
                char text[100];

                if(alloca->getAllocatedType()->isAggregateType()){

                    int def_id = get_def_id(trace,node);

                    if(def_id == -1){                        
                        outs() << "ISSUE: Definition of " << alloca->getName() << " is not present in trace, falling back to SVF id\n";
                        def_id = node->getId();
                    }

                    outs() << "Defs of param " << paramList[i]->getValueName() << " number " << i << " is " << node->getId() << " and has lenght " << 
                        evaluateSizeDL(alloca->getAllocatedType(),LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule(),TargetType::alloca_t) << "\n";

                    string name = "zref_" + string(m->getFunction(func_name)->getArg(i)->getName()) + "_id";
                    string value = "";
                    sprintf(text,"%d",def_id);
                    value.append(text);

                    callee->setMetadata(name,MDNode::get(ctx, MDString::get(ctx, value)));

                    break;
                } else
                    outs() << "Param " << i << " is not an array\n";

            }

        }
    }

    for(In *i : trace){
        
        // REFACTORING NEEDED

        for(auto it = i->storeUseBegin(); it != i->storeUseEnd(); it++){
            StoreUse *s = *it;
            Function *func = s->getStoreUse()->getFunction();
            string fname = string(func->getName());

            if(!ends_with(fname,"_instrumented"))
                continue;

            outs() << "Cheking store use inside " << fname << "\n";
            s->getStoreUse()->print(outs());

            for(auto it = s->targetObjBegin(); it != s->targetObjEnd(); it++){
                Def *d = *it;
                llvm::Argument *arg;
                AllocaInst *al = d->getDefAlloca();
             
                outs() << "Considering alloca instruction:\n\t";
                al->print(outs());
                outs() << "\n";
                
                if(al->getAllocatedType()->isArrayTy()){
                    outs() << "Avoiding pre-calculated indirect link\n";
                    continue;
                }

                PAGNode *pg = pag->getPAGNode(pag->getValueNode(al));
                outs() << "Pag ID of alloca is " << pg->getId() << "\n";

                for(auto it = pg->getIncomingEdgesBegin(PAGEdge::Store); it != pg->getIncomingEdgesEnd(PAGEdge::Store); it++){
                    StorePE *se = SVFUtil::cast<StorePE>(*it);
                    Value *store_src = const_cast<Value *>(se->getSrcNode()->getValue());

                    if((arg = check_parameter_aliasing(store_src,func,pta)) != NULL){
                        outs() << "Aliased with " << arg->getName() << "\n";

                        string name = "ziuse" ;
                        string value = string((*arg).getName()) + "_id";
                        s->getStoreUse()->setMetadata(name,MDNode::get(ctx, MDString::get(ctx, value)));

                        break;
                    }
                }               
            }
        }

        for(auto it = i->callLibUseBegin(); it != i->callLibUseEnd(); it++){
            CallLibUse *s = *it;
            Function *func = s->getCall()->getFunction();
            string fname = string(func->getName());

            if(!ends_with(fname,"_instrumented"))
                continue;

            outs() << "Cheking store use inside " << fname << "\n";
            s->getCall()->print(outs());
            
            for(auto it = s->targetObjBegin(); it != s->targetObjEnd(); it++){
                Def *d = *it;
                llvm::Argument *arg;
                AllocaInst *al = d->getDefAlloca();
             
                outs() << "Considering alloca instruction:\n\t";
                al->print(outs());
                outs() << "\n";
                
                if(al->getAllocatedType()->isArrayTy()){
                    outs() << "Avoiding pre-calculated indirect link\n";
                    continue;
                }

                PAGNode *pg = pag->getPAGNode(pag->getValueNode(al));
                outs() << "Pag ID of alloca is " << pg->getId() << "\n";

                for(auto it = pg->getIncomingEdgesBegin(PAGEdge::Store); it != pg->getIncomingEdgesEnd(PAGEdge::Store); it++){
                    StorePE *se = SVFUtil::cast<StorePE>(*it);
                    Value *store_src = const_cast<Value *>(se->getSrcNode()->getValue());

                    if((arg = check_parameter_aliasing(store_src,func,pta)) != NULL){
                        outs() << "Aliased with " << arg->getName() << "\n";

                        string name = "ziuse" ;
                        string value = string((*arg).getName()) + "_id";
                        s->getCall()->setMetadata(name,MDNode::get(ctx, MDString::get(ctx, value)));

                        break;
                    }
                }               
            }
        }
    }
    outs() << "---------------------------------------------------" << "\n";

    string ll_name = string(argv[1]);

    pag->dump(ll_name + ".pag");
    svfg->dump(ll_name + "." + ptaName + ".svfg");
    icfg->dump(ll_name + ".icfg");

    // clean up memory
    delete vfg;
    delete svfg;
    PAG::releasePAG();

    LLVMModuleSet::getLLVMModuleSet()->dumpModulesToFile(".svf.bc");
    
    std::string str;
    raw_string_ostream rawstr(str);

    return 0;
}

