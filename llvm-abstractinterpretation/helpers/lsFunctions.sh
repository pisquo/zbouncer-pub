#!/bin/bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [[ -z $optdir ]]
then
    optdir=$(dirname $(which llvm-nm))
fi
# clang-query -c="match cxxMethodDecl()" "$1" --
# clang-query -c="match functionDecl()" "$1" --
# # set traversal IgnoreUnlessSpelledInSource

definednames=$($optdir/llvm-nm "${1/.ll/.bc}" --defined-only | awk '{ print $3; }' )

for n in $definednames ; do
    plaintext=$(echo $n|c++filt| grep -v typeinfo | grep -v vtable)
    if [[ -z $plaintext ]]
    then
        continue
    fi

    "$SCRIPT_DIR"/flowgraph.sh "$1" "$n"
    #echo $n 
done
# | grep -v typeinfo | grep -v vtable