#!/bin/bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [[ -z $optdir ]]
then
    optdir=$(dirname $(which llvm-nm))
fi
$optdir/clang -cc1 -fdump-vtable-layouts -emit-llvm "$1" > "$2.classinfo"
$optdir/clang -cc1 -x c++ -v -fdump-record-layouts -emit-llvm "$1" >> "$2.classinfo"