#!/bin/bash
if [[ -z $optdir ]]
then
    echo setting opt to default
    optdir=$(dirname $(which opt))
fi
plaintext=$(echo "$2"|c++filt)
$optdir/opt --dot-cfg -cfg-func-name="$2" "$1" -disable-output
cat ".$2".dot | c++filt | dot -Tsvg > "$1.$plaintext.svg"
rm ".$2".dot