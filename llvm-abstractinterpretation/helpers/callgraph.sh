#!/bin/bash
if [[ -z $optdir ]]
then
    echo setting opt to default
    optdir=$(dirname $(which opt))
fi
$optdir/opt -enable-new-pm=0 -dot-callgraph -S "$1" -disable-output
cat "$1".callgraph.dot | c++filt | sed 's,>,\\>,g; s,-\\>,->,g; s,<,\\<,g' | dot -Tsvg > "$1.staticcallgraph.svg"
#dot -Tsvg output/classes.ll.callgraph.dot > classes.svg