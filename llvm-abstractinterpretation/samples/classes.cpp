class A {
    public:
    int a;
    virtual int f(){ return 0; };
    virtual int g(){ return 0; };
};

class B: public A {
    public:
    int b;
    virtual int f() { return 42; };
};

class D : public A {
    public:
    int d;
    virtual int  g(){ return 4711; };
};

class C: public B, public D {
    public:
    int c;
};

int main (void)
{
    int res = 0;
    A *a;
    if (a==0)
        a=new A();
    else
        a = new B();
    res += a->f();
    a = dynamic_cast<D*>(new C());
    res += a->g();
    return res;
}