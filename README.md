![zbouncer](static/zbouncer.gif)
[![pipeline status](https://gitlab.com/teozoia/zbouncer/badges/master/pipeline.svg)](https://gitlab.com/teozoia/zbouncer/-/commits/master)

# WARNING
Questa branch contiene il WIP del lavoro di @pisquo.

# zbouncer
Binary protection against ROP attacks enforced with ARM trustzone.

## Getting started
For reproduce all the enviorment you need to install: llvm, optee-os, optee-client, optee-example... and optee-armv7-qemu. You can choose to use our docker images teozoia/llvm-cross-x86_64-arm and teozoia/optee32.

You can run the example `hello.c` inside the main directory by follow these steps:

_Generate library wrapper_
* `your-pc$ docker run -it -v $PWD:/root/zbouncer --rm teozoia/optee32:latest`
* `docker-optee32$ cd ~/zbouncer/build`
* `docker-optee32$ make optee32-lib`

_Wraps all input function_
* `your-pc$ docker run -it --rm -v $PWD/:/root/zbouncer teozoia/llvm-cross-x86_64-arm:latest /bin/bash`
* `docker-llvm$ cd ~/zbouncer/build`
* `docker-llvm$ make instr-wrapper`

_Copy the output zb_in.ll inside SVF root folder_

_Execute svf-ex.cpp algorithm to find the WODI_

_Binary test instrumentation_
* `your-pc$ docker run -it --rm -v $PWD/:/root/zbouncer teozoia/llvm-cross-x86_64-arm:latest /bin/bash`
* `docker-llvm$ cd ~/zbouncer/build`
* `docker-llvm$ make instr-svf-wrapped`

_Execute instrumented binary inside qemu-optee-os arm32_
* `your-pc$ docker run -it --network=host --env DISPLAY=$DISPLAY --privileged --volume="$HOME/.Xauthority:/root/.Xauthority:rw" -v /tmp/.X11-unix:/tmp/.X11-unix -v $PWD:/root/zbouncer --rm teozoia/optee32:latest`
* `docker-optee32$ cd ~/zbouncer/build`
* `docker-optee32$ make optee32-run`

Now you can execute your binary: inside normal world console (username root, no password) just type `hello.instr` or `hello.native`.

`hello.c` is just an example, if you want to try your own code with zbouncer you simply substituite `hello.c` with your program and change the `build/Makefile` as you prefer. 

<hr>

Run test:
_Generate library wrapper_
* `your-pc$ docker run -it -v $PWD:/root/zbouncer --rm teozoia/optee32:latest`
* `docker-optee32$ cd ~/zbouncer/build`
* `docker-optee32$ make optee32-lib`

_LLVM compile all tests_
* `your-pc$ docker run -it --rm -v $PWD/:/root/zbouncer teozoia/llvm-cross-x86_64-arm:latest /bin/bash`
* `docker-llvm$ cd ~/zbouncer/test`
* `docker-llvm$ make test-compile`

_SVF analysis on your machine (you need to install SVF before)_
* `your-pc$ cd Zbouncer/test` (directory which contains tests)
* `your-pc$ make instr-svf` (You need to specify path of SVF in Zbouncer/test/Makefile)

_Instrumentation with tz_
* `your-pc$ docker run -it --rm -v $PWD/:/root/zbouncer teozoia/llvm-cross-x86_64-arm:latest /bin/bash`
* `docker-llvm$ cd ~/zbouncer/test_svf`
* `docker-llvm$ make instr-zbouncer`

* Uncomment or add the path of you executable in Zbouncer/build/Makefile recipe optee32-compile

_Execute instrumented binary inside qemu-optee-os arm32_
* `your-pc$ docker run -it --network=host --env DISPLAY=$DISPLAY --privileged --volume="$HOME/.Xauthority:/root/.Xauthority:rw" -v /tmp/.X11-unix:/tmp/.X11-unix -v $PWD:/root/zbouncer --rm teozoia/optee32:latest`
* `docker-optee32$ cd ~/zbouncer/build`
* `docker-optee32$ make optee32-run`
