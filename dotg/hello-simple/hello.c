#include <stdio.h>
#include <stdlib.h>    
#include <err.h>
#include <unistd.h>
#include <string.h>

void func1(int num){  //num=1000 
    for(int i = 0; i < num; i++){
        printf("%d\n", i);
    }
}

void myfunc(){
    char buf[20];
  
    int c = 100;
    int i = 0;
    while (c < 0x20) {
        c = fgetc(stdin);
        buf[i] = c;
        i++;
    }
}


int main(int argc, char *argv[]){

	func1(1000);
    myfunc();

    return 0;
}