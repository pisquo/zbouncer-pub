#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>


#include <sys/socket.h>
#include <arpa/inet.h>

/* OP-TEE TEE client API (built by optee_client) */
#include <tee_client_api.h>

/* For the UUID (found in the TA's h-file(s)) */
#include "libzbouncer_ta.h"

#include "libzbouncer.h"

#define MAPS_FMT "%08lx-%08lx %c%c%c%c %08llx %02x:%02x %lu %s"
#define DUMP_FMT "%ld_%s_%08lx.rawdmp"

extern int errno ;
pid_t parent_pid; 

static TEEC_SharedMemory shared = {0};
pid_t child;

typedef struct use{
	uint32_t def_addr;
	uint16_t def;
    uint32_t use_addr;
	uint16_t use_id;
	uint16_t i_use;
}use_t;




int res_open;
char *line = NULL;
size_t len = 0, rd;
FILE *fp = NULL;


/*
    * START ZBouncer Trace sending Functions
*/
/*
    * Open a socket to send zb trace out
*/

int open_socket(char *addr, uint16_t port){
    printf("[!] Open Socket\n");
    printf("[*] Address: %s\n", addr);
    printf("[*] Port: %u\n", port);
    int sock = 0;
    struct sockaddr_in serv_addr;
    //char buffer[1024] = {0};
    if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0){
        printf("\n Socket creation error \n");
        return -1;
    }
   
    serv_addr.sin_family = PF_INET;
    serv_addr.sin_port = htons(port);
       
    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(PF_INET, addr, &serv_addr.sin_addr)<=0){
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
        printf("\nConnection Failed \n");
        return -1;
    }
    //send(sock,trace,1024,0);
    //printf("Hello message sent\n");
    //valread = read(sock,buffer,1024);
    //printf("%s\n",buffer );

    //close(sock);
    return sock;
}

/*
    * Function to send trace to verifier via socket 
*/
void send_trace_to_verifier(){
    if((res_open =  open_socket("172.17.0.1", 8080)) < 0){
            printf("[*] Error in opening Socket\n");
            exit(0);
    }
    if ( (fp = fopen("trace.txt", "r")) == NULL){
        perror ("The following error occurred");
        printf("Value of errno: %d\n", errno);
    }
    else{
        while((rd = getline(&line, &len, fp)) != -1){
            send(res_open, line, strlen(line) ,0);
        }
        free(line);
        fclose(fp);   
    }
    close(res_open);
}

/*
    * Function to write ztrace from shared mem to a file
*/
void  write_trace_to_file(){
    if ( (fp = fopen("trace.txt", "w")) == NULL){
        perror ("The following error occurred");
        printf("Value of errno: %d\n", errno);
    }
    else{
        for(int i = 0; i < 1024 ; i++){
            if(((use_t*)shared.buffer)[i].def != 0)
                fprintf(fp, "DEF | %05u | %08x\n",  ((use_t*)shared.buffer)[i].def, ((use_t*)shared.buffer)[i].def_addr);
            if(((use_t*)shared.buffer)[i].use_id != 0)
                fprintf(fp, "USE | %05u | %08x\n", ((use_t*)shared.buffer)[i].use_id, ((use_t*)shared.buffer)[i].use_addr);
        }
        fclose(fp);
        send_trace_to_verifier();
    } 
}

void z_extractor(pid_t cpid){
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_WHOLE, TEEC_NONE, TEEC_NONE, TEEC_NONE);
    shared.flags = TEEC_MEM_INPUT | TEEC_MEM_OUTPUT;
    shared.buffer = calloc(1024, sizeof(use_t));
    shared.size = 50*sizeof(int);

    op.params[0].memref.parent = &shared;
    op.params[0].memref.size = shared.size;

    //register a memory through the GlobalPlatfor
    //Client API function TEEC_RegisterSharedMemory
    if (TEEC_RegisterSharedMemory(&ctx, &shared) != TEEC_SUCCESS){
        free(shared.buffer);
        printf("[HOST] Error RegisterSharedMemory\n");
    } 
        
    printf("memory allocated : %p\n", shared);
    while(1){
        sleep(20);
        printf("I am in the lopp ;=)\n");
        if(cpid != parent_pid){
            res = TEEC_InvokeCommand(&sess, TA_ZBOUNCER_CMD_GOTVALUE, &op, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x\n", res, err_origin);
            // call ta side to get trace
            write_trace_to_file();
            exit(0);
        }
        res = TEEC_InvokeCommand(&sess, TA_ZBOUNCER_CMD_GOTVALUE, &op, &err_origin);
        if (res != TEEC_SUCCESS)
            errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x\n", res, err_origin);
        // call ta side to get trace
        write_trace_to_file();
    }

}

void zinit(){

    res = TEEC_InitializeContext(NULL, &ctx);
    if (res != TEEC_SUCCESS)
        errx(1, "TEEC_InitializeContext failed with code 0x%x\n", res);
    
    res = TEEC_OpenSession(&ctx, &sess, &uuid, TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
    if (res != TEEC_SUCCESS)
        errx(1, "TEEC_Opensession failed with code 0x%x origin 0x%x\n", res, err_origin);

    printf("ZINIT\n\n\n");

    parent_pid = getpid();
    child = fork();
    if (child == 0){
        pid_t child_pid = getpid();
        printf("I am the Child %d\n", child_pid);
        z_extractor(child_pid);
    }
    
}

/*

    * END ZBouncer Trace sending Functions

*/


void zbouncer_collect_alloca(uint16_t def, char* addr){

    op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INOUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);
    op.params[0].value.a = def;
    op.params[0].value.b = (uint32_t)addr;

    res = TEEC_InvokeCommand(&sess, TA_ZBOUNCER_DEF, &op, &err_origin);
    if (res != TEEC_SUCCESS)
        errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x\n", res, err_origin);
}

void zbouncer_use(uint16_t use, char* addr){

    op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INOUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);
    op.params[0].value.a = use;
    op.params[0].value.b = (uint32_t) addr;

    res = TEEC_InvokeCommand(&sess, TA_ZBOUNCER_USE, &op, &err_origin);
    if (res != TEEC_SUCCESS)
        errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x\n", res, err_origin);
}

uint32_t getEndOfObject(char *ptr){
  
  char c = *ptr;
  while(c != '\0'){
    ++ptr;
    c = *ptr;
  }

  return (uint32_t)ptr;
}

void zbouncer_luse(uint16_t use, char* addr, uint32_t size_of_write){

    if(size_of_write == 0xdeadbeef){
        zbouncer_use(use, addr + getEndOfObject(addr));
    }else{
        zbouncer_use(use, addr + size_of_write);
    }
}

void zbouncer_iuse(uint16_t use, uint16_t def, char* addr){
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_WHOLE, TEEC_NONE, TEEC_NONE, TEEC_NONE);

    uint32_t *data = calloc(3, sizeof(uint32_t));
    data[0] = use;
    data[1] = def;
    data[2] = (uint32_t) addr;

    shared.flags = TEEC_MEM_INPUT | TEEC_MEM_OUTPUT;
    shared.buffer = data;
    shared.size = 3*sizeof(uint32_t);

    op.params[0].memref.parent = &shared;
    op.params[0].memref.size = shared.size;

    if (TEEC_RegisterSharedMemory(&ctx, &shared) != TEEC_SUCCESS){
        free(shared.buffer);
        printf("[HOST] Error RegisterSharedMemory\n");
    }

    printf("SENDING IUSE: %05d | %05d | %08x\n", ((uint32_t *) shared.buffer)[0], ((uint32_t *) shared.buffer)[1], ((uint32_t *) shared.buffer)[2]);
    res = TEEC_InvokeCommand(&sess, TA_ZBOUNCER_IUSE, &op, &err_origin);
    if (res != TEEC_SUCCESS)
        errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x\n", res, err_origin);
}

void zbouncer_iluse(uint16_t use, uint16_t def, char* addr, uint32_t size_of_write){
     if(size_of_write == 0xdeadbeef){
        zbouncer_iuse(use, def, addr + getEndOfObject(addr));
    }else{
        zbouncer_iuse(use, def, addr + size_of_write);
    }
}
