#include <iostream>

#define HIGH 1
#define LOW 0
#define OUTPUT 1

#define SDA 12
#define SCL 13

uint8_t pins_state[1024];
uint32_t pins[1024];

void pinMode(int pin, uint32_t value);
void digitalWrite(int pin, uint32_t value);

bool startWaveform(int pin, int v1, int refresh_interval, int v2);
bool startWaveform(int pin, int v1, int refresh_interval, int v2, int phaseReference);
void delay(int refresh_interval); // long enough to complete active period under all circumstances.
bool stopWaveform(int pin);
int max(int v1, int v2);
int min(int v1, int v2);
int constrain(int value, int v1, int v2);

