// Gabriel Waltrip
// Rover.cpp : Defines the entry point for the console application.
// gcc -o rover.out -lwiringPi -lm -pthread rover.c Compass.h gps.h
#include <stdio.h>
#include <pthread.h>

#define MOTOR_RIGHT_A	0
#define MOTOR_RIGHT_B	2
#define MOTOR_LEFT_A	3
#define MOTOR_LEFT_B	4

#define Stop_All_Motors()	digitalWrite(MOTOR_RIGHT_A,0);\
				digitalWrite(MOTOR_RIGHT_B,0);\
				digitalWrite(MOTOR_LEFT_A,0);\
				digitalWrite(MOTOR_LEFT_B,0);


#ifndef TCP_C
#define TCP_C
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

char __attribute__((annotate("sensitive"))) mode;

void tcpError(const char *msg) {
    perror(msg);
    exit(1);
}

void leggi(char *buffer){

	scanf("%s", buffer);
	printf("You entered: %s", buffer);

}

void *tcpListener(void *arg){
	char buffer[20];
	leggi(buffer);
	
	return NULL;
}
#endif


void digitalWrite(int pin, int value) {
//	printf("%s (int pin = %d, int value = %d)\n", __func__, pin, value);
	return;
}

int main(int argc, char **argv)
{
	mode = 0xff;
	char __attribute__((annotate("sensitive"))) last = mode;
	int count = 0;
	unsigned long start, end;

	//pthread_t tcp;
	//pthread_create(&tcp, NULL,tcpListener,"");


	/*Starts Main Loop*/
	printf("Starting Mainloop!\n");
    
       printf("%s %d\n",__func__, __LINE__);
	while (count++ < 1) {
       printf("%s %d\n",__func__, __LINE__);
		tcpListener(NULL);
       printf("%s %d\n",__func__, __LINE__);
		if(last == mode){
			Stop_All_Motors();
			last = mode;
		}
		//Fordward
		else
		if(mode == 0x1+'0'){
			printf("Forward\n");
			digitalWrite(MOTOR_RIGHT_A,1);
			digitalWrite(MOTOR_LEFT_A,1);
		}
		//Backwards
		else if(mode == 0x2 + '0'){
			printf("Backward\n");
			digitalWrite(MOTOR_RIGHT_B,1);
			digitalWrite(MOTOR_LEFT_B,1);
		}
		//Left
		else if (mode == 0x3 + '0'){
			printf("Left\n");
			digitalWrite(MOTOR_RIGHT_A,1);
			digitalWrite(MOTOR_LEFT_B,1);
		}
		//Right
		else if (mode == 0x4 + '0'){
			printf("Right\n");
			digitalWrite(MOTOR_RIGHT_B,1);
			digitalWrite(MOTOR_LEFT_A,1);
		}

	}


	return 0;
}
