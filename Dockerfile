FROM debian:buster-slim AS cross_common

# Make sure the image is updated, install some prerequisites,
RUN apt-get update \
  && dpkg --add-architecture armhf \
  && apt-get update \
  && apt-get install -y qemu-user-static \
  && apt-get install -y build-essential \
  wget xz-utils cmake git python3-dev libncurses5-dev libxml2-dev \
  libedit-dev swig doxygen graphviz xz-utils ninja-build ssh \
  && apt-get install -y crossbuild-essential-armhf libpython3-dev:armhf \
  libncurses5-dev:armhf libxml2-dev:armhf libedit-dev:armhf \
  && rm -rf /var/lib/apt/lists/*

FROM cross_common AS cross_builder
# Get the LLVM sources
  RUN cd ~ && mkdir llvm_all && cd llvm_all \
  && wget https://releases.llvm.org/9.0.0/llvm-9.0.0.src.tar.xz \
  && tar xf llvm-9.0.0.src.tar.xz \
  && rm llvm-9.0.0.src.tar.xz \
  && mv llvm-9.0.0.src llvm \
  && cd ~/llvm_all/llvm/tools \
  && wget https://releases.llvm.org/9.0.0/cfe-9.0.0.src.tar.xz \
  && tar xf cfe-9.0.0.src.tar.xz \
  && rm cfe-9.0.0.src.tar.xz \
  && mv cfe-9.0.0.src clang \
  && cd ~/llvm_all/llvm/projects \
  && wget https://releases.llvm.org/9.0.0/compiler-rt-9.0.0.src.tar.xz \
  && wget https://releases.llvm.org/9.0.0/lld-9.0.0.src.tar.xz \
  && wget https://releases.llvm.org/9.0.0/polly-9.0.0.src.tar.xz \
  && wget https://releases.llvm.org/9.0.0/libunwind-9.0.0.src.tar.xz \
  && tar xf compiler-rt-9.0.0.src.tar.xz \
  && tar xf lld-9.0.0.src.tar.xz \
  && tar xf polly-9.0.0.src.tar.xz \
  && tar xf libunwind-9.0.0.src.tar.xz \
  && mv compiler-rt-9.0.0.src compiler-rt \
  && mv lld-9.0.0.src lld \
  && mv polly-9.0.0.src polly \
  && mv libunwind-9.0.0.src libunwind \
  && rm compiler-rt-9.0.0.src.tar.xz \
  && rm lld-9.0.0.src.tar.xz \
  && rm polly-9.0.0.src.tar.xz \
  && rm libunwind-9.0.0.src.tar.xz \
  && cd ~/llvm_all \
  && wget https://releases.llvm.org/9.0.0/libcxx-9.0.0.src.tar.xz \
  && wget https://releases.llvm.org/9.0.0/libcxxabi-9.0.0.src.tar.xz \
  && wget https://releases.llvm.org/9.0.0/openmp-9.0.0.src.tar.xz \
  && tar xf libcxx-9.0.0.src.tar.xz \
  && tar xf libcxxabi-9.0.0.src.tar.xz \
  && tar xf openmp-9.0.0.src.tar.xz \
  && mv libcxx-9.0.0.src libcxx \
  && mv libcxxabi-9.0.0.src libcxxabi \
  && mv openmp-9.0.0.src openmp \
# Build and install LLVM and armhf
  && cd ~/llvm_all && mkdir build_llvm && cd build_llvm \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DLLVM_BUILD_DOCS=OFF -DCMAKE_INSTALL_PREFIX=/usr/local/cross_armhf_clang_9.0.0 -DCMAKE_CROSSCOMPILING=True -DLLVM_DEFAULT_TARGET_TRIPLE=arm-linux-gnueabihf -DLLVM_TARGET_ARCH=ARM -DLLVM_TARGETS_TO_BUILD=ARM ../llvm \
  && ninja \
  && ninja install \
  && echo 'export PATH=/usr/local/cross_armhf_clang_9.0.0/bin:$PATH' >> ~/.bashrc \
  && echo 'export LD_LIBRARY_PATH=/usr/local/cross_armhf_clang_9.0.0/lib:$LD_LIBRARY_PATH' >> ~/.bashrc \
  && . ~/.bashrc \
  && cd ~/llvm_all \
  && mkdir build_libcxxabi && cd build_libcxxabi \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local/cross_armhf_clang_9.0.0 -DLLVM_TARGETS_TO_BUILD=ARM -DCMAKE_C_COMPILER=/usr/local/cross_armhf_clang_9.0.0/bin/clang -DCMAKE_CXX_COMPILER=/usr/local/cross_armhf_clang_9.0.0/bin/clang++ ../libcxxabi \
  && ninja \
  && ninja install \
  && cd ~/llvm_all \
  && mkdir build_libcxx && cd build_libcxx \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local/cross_armhf_clang_9.0.0 -DLLVM_TARGETS_TO_BUILD=ARM -DCMAKE_C_COMPILER=/usr/local/cross_armhf_clang_9.0.0/bin/clang -DCMAKE_CXX_COMPILER=/usr/local/cross_armhf_clang_9.0.0/bin/clang++ ../libcxx \
  && ninja \
  && ninja install \
  && cd ~/llvm_all \
  && mkdir build_openmp && cd build_openmp \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local/cross_armhf_clang_9.0.0 -DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc -DCMAKE_CXX_COMPILER=arm-linux-gnueabihf-g++ -DLIBOMP_ARCH=arm ../openmp \
  && ninja \
  && ninja install

FROM cross_common
  COPY --from=cross_builder /usr/local/cross_armhf_clang_9.0.0 /usr/local/cross_armhf_clang_9.0.0
  RUN echo 'export PATH=/usr/local/cross_armhf_clang_9.0.0/bin:$PATH' >> ~/.bashrc \
  && echo 'export LD_LIBRARY_PATH=/usr/local/cross_armhf_clang_9.0.0/lib:$LD_LIBRARY_PATH' >> ~/.bashrc \
  && . ~/.bashrc

# Start from a Bash prompt
CMD [ "/bin/bash" ]
