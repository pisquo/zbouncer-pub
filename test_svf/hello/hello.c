#include <stdio.h>
#include <stdlib.h>    
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

void func1(int num){  //num=1000 
    for(int i = 0; i < num; i++){
        printf("%d\n", i);
    }
}

void myfunc(){
    char buf[20];

    int c = 100;
    int i = 0;
    while(c > 0x20){
        c = fgetc(stdin);
        buf[i] = c;
        i++;
    }
}

int main(int argc, char *argv[]){

    clock_t tic = clock();

	func1(1000);
    myfunc();

    clock_t toc = clock();
    printf("Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);

    return 0;
}
