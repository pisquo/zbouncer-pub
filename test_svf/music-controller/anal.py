import angr, pyvex, r2pipe
from pprint import pprint

def run_command(rad_inst,cmd):
    return rad_inst.cmd(cmd)

def get_lines(rad_inst,res):
    function_list = run_command(rad_inst,"afl")

    ret = []
    
    for entry in res.split("\n"):
        s = entry.split()

        if len(s) < 2:
            continue

        if s[0] + "_instrumented" not in function_list:
            ret.append(entry)
    
    return ret


def get_func_to_instr():
    zbouncer_funcs = ["zinit","zbouncer_collect_alloca","zbouncer_use","zbouncer_luse","zbouncer_iuse","zbouncer_iluse"]
    r = r2pipe.open("music-controller.wrapped.svf.deploy.exec")
    
    run_command(r,"aaaa")
    
    func_address = dict()
    
    for f in zbouncer_funcs:
        run_command(r,"s sym." + f)
        res = run_command(r,"axt")
    
        func_address[f] = []
        
        for ref in get_lines(r,res):
    
            s = ref.split()
            
            if(len(s) < 2):
                continue
    
            if s[0].strip() in zbouncer_funcs:
                continue
    
            func_address[f].append(int(s[1].strip(),16))
    
    pprint(func_address)
    return func_address

if __name__ == "__main__":
    get_func_to_instr()






