import angr, claripy, r2pipe, pyvex
import sys
from pprint import pprint

p = angr.Project('music-controller.wrapped.svf.deploy.exec', main_opts={'backend':'elf', 'arch': 'armhf', 'base_addr': 0x0}, auto_load_libs=False)
solution = []

zbouncer_funcs = ["zinit","zbouncer_collect_alloca","zbouncer_use","zbouncer_luse","zbouncer_iuse","zbouncer_iluse"]


def run_command(rad_inst,cmd):
    return rad_inst.cmd(cmd)

def get_lines(rad_inst,res):
    function_list = run_command(rad_inst,"afl")
    ret = []
    
    for entry in res.split("\n"):
        s = entry.split()

        if len(s) < 2:
            continue

        if s[0] + "_instrumented" not in function_list:
            ret.append(entry)
    
    return ret


def get_func_to_instr():
    r = r2pipe.open("music-controller.wrapped.svf.deploy.exec")
    
    run_command(r,"aaaa")
    func_address = dict()
    
    for f in zbouncer_funcs:
        run_command(r,"s sym." + f)
        res = run_command(r,"axt")
        func_address[f] = []
        for ref in get_lines(r,res):
            s = ref.split()
            if(len(s) < 2):
                continue
    
            if s[0].strip() in zbouncer_funcs:
                continue
    
            func_address[f].append(int(s[1].strip(),16))
    
    return func_address


def write_to_file(p, solution):
    with open(p.filename+"_input", "a") as f:
        for item in solution:
            word = str(item).replace('b','').replace('\'','')
            print(word)
            f.write(word +'\n')

    f.close()


def myfunc():

    input = claripy.BVS('input', 64)
    init_state = p.factory.full_init_state(
          args=['./music-controller.wrapped.svf.deploy.exec'],
          stdin=input,
    )


    sm = p.factory.simgr(init_state)

    pprint({x[0] : [hex(y) for y in x[1]] for x in get_func_to_instr().items()})

    for f in zbouncer_funcs:
        for address in get_func_to_instr()[f]:
            print(f"Finding input for {f} : {hex(address)}")
            sm.explore(find=address)
            found = sm.found[0]
            solution = found.solver.eval_upto(input, 10, cast_to=bytes)
    #print(repr(solution))

    #write angr out to file
            write_to_file(p, solution)

if __name__ == '__main__':
    #get_func_to_instr()
    myfunc();

