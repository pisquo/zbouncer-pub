#include <stdlib.h>
#include <stdio.h>

#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include <zbouncer_ta.h>

#define DYNAMIC_TRACE_SIZE 1024

typedef struct use{
	uint32_t def_addr;
	uint16_t def;
    uint32_t use_addr;
	uint16_t use_id;
	uint16_t i_use;
}use_t;

use_t dynamic_trace[DYNAMIC_TRACE_SIZE];
int trace_id;

int random = 0;

static use_t *mshared = NULL;

TEE_Result TA_CreateEntryPoint(void){
	DMSG("TA_CreateEntryPoint has been called");
	return TEE_SUCCESS;
}

void TA_DestroyEntryPoint(void){
	DMSG("TA_DestroyEntryPoint has been called");
}

TEE_Result TA_OpenSessionEntryPoint(uint32_t param_types,
		TEE_Param __maybe_unused params[4],
		void __maybe_unused **sess_ctx){

	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("TA_OpenSessionEntryPoint has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	(void)&params;
	(void)&sess_ctx;

	trace_id = 0;
	IMSG("[OK] init\n");
	return TEE_SUCCESS;
}

void TA_CloseSessionEntryPoint(void __maybe_unused *sess_ctx){
	(void)&sess_ctx;
	IMSG("TA_CloseSessionEntryPoint hash been called\n");
}

static TEE_Result zbouncer_def(uint32_t param_types, TEE_Param params[4]){
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INOUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	dynamic_trace[trace_id].def = params[0].value.a;
	dynamic_trace[trace_id].def_addr = params[0].value.b;
	trace_id++;
	IMSG("DEF | %05d | %08x\n", params[0].value.a, params[0].value.b);


	return TEE_SUCCESS;
}

static TEE_Result zbouncer_use(uint32_t param_types, TEE_Param params[4]){
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INOUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS; 

	dynamic_trace[trace_id].use_id = params[0].value.a;
	dynamic_trace[trace_id].use_addr = params[0].value.b;
	trace_id++;

	IMSG("USE | %05d | %08x\n", params[0].value.a, params[0].value.b);

	return TEE_SUCCESS;
}

static TEE_Result zbouncer_iuse(uint32_t param_types, TEE_Param params[4]){
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INOUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	/*dynamic_trace[trace_id].use_id = params[0].value.a;
	dynamic_trace[trace_id].addr = params[0].value.b;
	trace_id++; */
	
	uint32_t *data = params[0].memref.buffer;
	IMSG("IUSE | %05d | %05d | %08x\n", data[0], data[1], data[2]);

	return TEE_SUCCESS;
}


static TEE_Result copy_trace(uint32_t param_types, TEE_Param params[4]){
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INOUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	TEE_MemMove(params[0].memref.buffer, dynamic_trace, 10*sizeof(use_t));

	return TEE_SUCCESS;
}

TEE_Result TA_InvokeCommandEntryPoint(void __maybe_unused *sess_ctx,
			uint32_t cmd_id, uint32_t param_types, TEE_Param params[4]){
	(void)&sess_ctx;

	//IMSG("switch param: %d", cmd_id);
	switch (cmd_id) {
		case TA_ZBOUNCER_USE:
			return zbouncer_use(param_types, params);
		case TA_ZBOUNCER_DEF:
			return zbouncer_def(param_types, params);
		case TA_ZBOUNCER_CMD_GOTVALUE:
			return copy_trace(param_types, params);
		case TA_ZBOUNCER_IUSE:
			return zbouncer_iuse(param_types, params);
		default:
			return TEE_ERROR_BAD_PARAMETERS;
	}
}
