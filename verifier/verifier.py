import threading, queue, socket, sys, re

from pprint import pprint

import llvmlite.binding as llvm

class Trace_line:
    ident = 0
    def __init__(self,traceline):
        values = traceline.split(";")

        self.id = int(values[0])
        self.type = values[1]
        self.father = int(values[2]) if values[2] != '' else None
        self.length = int(values[3]) if len(values) > 4 else 0
        self.info = values[-1]
        self.children = []
        self.address = None

    def __repr__(self):
        from pprint import pformat
        out = ("{type}:\n{ident_base}- Id: {id}" + \
              "\n{ident_base}- Info:{info}"+ \
              "\n{ident_base}- Father: {father}"
              "\n{ident_base}- Len: {length}"+ \
              "\n{ident_base}- Children:\n{ident_children}" ).format(\
                   ident_base = "\t" * (Trace_line.ident + 1), \
                   type = self.type, id = self.id, info = self.info, \
                   length = self.length if self.length else 0, \
                   ident_children="\t" * (Trace_line.ident + 2), \
                   father = self.father)
            #f"{self.type}:\n{'\t' * Trace_line.ident + 1  }- Id: {self.id}\n{'\t' * Trace_line.ident + 1}- Info:{self.info}\n{'\t' * Trace_line.ident + 1}- Len: {self.length if self.length else 0}\n{'\t' * Trace_line.ident + 1}- Children:\n{'\t' * Trace_line.ident  + 2}"

        Trace_line.ident += 1
        out += pformat(self.children,indent = 3 + Trace_line.ident)
        Trace_line.ident -= 1

        return out



data_queue = queue.Queue()

def_map = dict()
static_trace = []
nodes = []
mod = None

def trace_receiver(port, ip):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      s.bind((ip,port))
      s.listen()

      while True:
        conn,addr = s.accept()
        data = b""
        with conn:
          while True:
            d = conn.recv(1024)
            if not d:
              break
            data += d
          data_queue.put(data)
          conn.close()

# def leading_tabs(line):
#     return len(line) - len(line.lstrip("\t"))

def trace_parser(trace):
    for line in trace.strip().split("\n"):
        tl = Trace_line(line.strip())
        nodes.append(tl)

        if "DEF" in tl.type:
            if tl.id in def_map.keys():
                def_map[tl.id].append(tl)
            else:
                def_map[tl.id] = [tl]

        if not tl.father:
            static_trace.append(tl)
        else:
            father = next(t for t in nodes if t.id == tl.father)
            father.children.append(tl)

def find_instr(info_line,llvm_mod):
    for f in llvm_mod.functions:
        for b in f.blocks:
            for i in b.instructions:
                instr =  re.sub(",\s+!z[isld].*$","",str(i)).strip()
                if instr in info_line:
                    return i


def check_use(def_struct,use_struct,end_address):
    print(f"Checking use of\n\t- Begin: {def_struct.address}\n\t- End: {end_address}\n\t- Len: {def_struct.length}")
    return (end_address - def_struct.address) > def_struct.length

def verify_input(trace):
  formatted_trace = [ [x.strip() for x in line.split("|")] for line in (str(trace)).strip().split("\n")]

  pprint(formatted_trace)

  for entry in formatted_trace:
      if entry[0] == "DEF":
          for df in def_map[int(entry[1])]:
              df.address = int(entry[2])

      if entry[0] == "USE":
          use_id = int(entry[1])
          end_addr = int(entry[2])

          use_struct = next(x for x in nodes if x.id == use_id)

          assert len(use_struct.children) == 1

          print(use_struct.children)

          def_struct = use_struct.children[0]

      elif entry[0] == "IUSE":
          use_id = int(entry[1])
          def_id = int(entry[2])
          end_addr = int(entry[3])

          use_struct = next( x for x in nodes if x.id == use_id )
          def_struct = def_map[def_id][0]

      if "USE" in entry[0] and check_use(def_struct, use_struct, end_addr):
          i_use = find_instr(use_struct.info,mod)
          i_def = find_instr(def_struct.info,mod)

          print(f"VIOLATION\nOverflow of data defined at {str(i_def)} in function {i_def.function.name} cause by instruction {i_use} in function {i_use.function.name}")

def main():
  global mod

  tr = threading.Thread(target=trace_receiver,args=(1234,"localhost"))
  tr.start()

  trace_parser(open(sys.argv[1],"r").read())
  mod = llvm.parse_assembly(open(sys.argv[2],"r").read())

  while True:
    trace = data_queue.get()
    print("[DEBUG] Received data: ", trace)

    verify_input(trace)


if __name__ == "__main__":
  #main()
  trace_parser(open(sys.argv[1],"r").read())
  mod = llvm.parse_assembly(open(sys.argv[2],"r").read())

  for inp in open("test_trace.txt","r").read().strip().split("\n"):
      print("Received line " + inp)
      verify_input(inp)
